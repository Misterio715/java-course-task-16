package org.fmavlyutov.component;

import org.fmavlyutov.api.controller.ICommandController;
import org.fmavlyutov.api.controller.IProjectController;
import org.fmavlyutov.api.controller.IProjectTaskConroller;
import org.fmavlyutov.api.controller.ITaskController;
import org.fmavlyutov.api.repository.ICommandRepository;
import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.*;
import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;
import org.fmavlyutov.controller.CommandController;
import org.fmavlyutov.controller.ProjectController;
import org.fmavlyutov.controller.ProjectTaskConroller;
import org.fmavlyutov.controller.TaskController;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.AbstractException;
import org.fmavlyutov.exception.system.CommandNotFoundException;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.repository.CommandRepository;
import org.fmavlyutov.repository.ProjectRepository;
import org.fmavlyutov.repository.TaskRepository;
import org.fmavlyutov.service.*;
import org.fmavlyutov.util.TerminalUtil;

import static org.fmavlyutov.constant.CommandLineConstant.*;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskConroller projectTaskController = new ProjectTaskConroller(projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    public void start(String[] args) {
        initData();
        initLogger();
        argumentsProcessing(args);
        commandsProcessing();
    }

    private void initData() {
        projectService.add(new Project("Some project", "this is project 1", Status.IN_PROGRESS));
        projectService.add(new Project("Another project", "this is project 2", Status.NOT_STARTED));
        projectService.add(new Project("Third project", "this is project 3", Status.COMPLETED));
        taskService.create("First task", "this is task 1");
        taskService.create("Second task", "this is task 2");
        taskService.create("Third task", "this is task 3");
    }

    private void initLogger() {
        loggerService.info("###### PROGRAM IS STARTING  ######");
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                loggerService.info("###### PROGRAM IS SHUTTING DOWN ######")));
    }

    private void argumentsProcessing(String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        for (String arg : args) {
            switch (arg) {
                case CommandLineArgument.HELP:
                    commandController.displayHelp();
                    break;
                case CommandLineArgument.VERSION:
                    commandController.displayVersion();
                    break;
                case CommandLineArgument.ABOUT:
                    commandController.displayAbout();
                    break;
                case CommandLineArgument.INFO:
                    commandController.displayInfo();
                    break;
                case CommandLineArgument.COMMANDS:
                    commandController.displayCommands();
                    break;
                case CommandLineArgument.ARGUMENTS:
                    commandController.displayArguments();
                    break;
                default:
                    commandController.displayArgumentError();
            }
        }
    }

    private void commandsProcessing() {
        String arg = "";
        while (!arg.equals(EXIT)) {
            arg = TerminalUtil.nextLine();
            try {
                displayCommand(arg);
                loggerService.command(arg);
                System.out.println("[SUCCESSFUL]\n");
            } catch (AbstractException e) {
                loggerService.error(e);
                System.out.println("[UNSUCCESSFUL: " + e.getLocalizedMessage() + "]\n");
            }
        }
    }

    private void displayCommand(String arg) {
        if (arg == null || arg.isEmpty()) {
            return;
        }
        switch (arg) {
            case CommandLineConstant.HELP:
                commandController.displayHelp();
                break;
            case CommandLineConstant.VERSION:
                commandController.displayVersion();
                break;
            case CommandLineConstant.ABOUT:
                commandController.displayAbout();
                break;
            case CommandLineConstant.INFO:
                commandController.displayInfo();
                break;
            case CommandLineConstant.COMMANDS:
                commandController.displayCommands();
                break;
            case CommandLineConstant.ARGUMENTS:
                commandController.displayArguments();
                break;
            case PROJECT_CREATE:
                projectController.createProject();
                break;
            case PROJECT_LIST:
                projectController.showProjects();
                break;
            case PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TASK_CREATE:
                taskController.createTask();
                break;
            case TASK_LIST:
                taskController.showTasks();
                break;
            case TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectId();
                break;
            case EXIT:
                exit();
                break;
            default:
                throw new CommandNotFoundException(String.format("Command not found. Enter [%s] to see all arguments", CommandLineArgument.HELP));
        }
    }

    private void exit() {
        System.exit(0);
    }

}
